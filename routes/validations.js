var t = require('tcomb-validation');

const reqqueryvalidation =(req, res, next) => {
 let result = t.validate(req.query, type);
 if (result.isValid()) next();
 else res.status(400).json(result.errors);
};

const reqpathvalidation = (req, res, next) => {
 let result = t.validate(req.path, type);
 if (result.isValid()) next();
 else res.status(400).json(result.errors);
};

const reqBodyValidation =(req, res, next) => {
 let result = t.validate(req.body, type);
 if (result.isValid()) next();
 else res.status(400).json(result.errors);
};

module.exports = { reqqueryvalidation, reqpathvalidation, reqBodyValidation };



// const StringNumber = t.refinement(t.String, s => !isNaN(s))