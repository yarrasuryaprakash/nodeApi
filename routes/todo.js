const express = require("express");
const router = express.Router();
const knex = require("knex");
const config = require("../knexfile").development;

// const count = todos.length;

router.post("/", (req, res) => {
    knex(config)
        .returning("*")
        .insert(req.body)
        .into("todos")
        .then(function(todos) {
            res.send(todos);
        })
        .error(e => res.status(500).json(e.message));
});

router.put("/:id", (req, res) => {
    knex(config)
        .from("todos")
        .where("id", req.params.id)
        .update(req.body)
        .returning("*")
        .then(function(todos) {
            res.status(201).json(todos);
        })
        .error(e => res.status(500).json(e.message));
});

router.delete("/:id", (req, res) => {
    knex(config)
        .where("id", req.params.id)
        .del()
        .from("todos")
        .returning(["id", "name", "isActive", "isDelete"])
        .then(function(todos) {
            res.json(todos);
        })
        .error(e => res.status(500).json(e.message));
});

router.get("/", (req, res) => {
    knex(config)
        .select("*")
        // .select("id", "name", "isActive", "isDelete")
        .where("isDelete", false)
        .from("todos")
        .then(function(todos) {
            res.send(todos);
        })
        .error(e => res.status(500).json(e.message));
});

router.get("/active", (req, res) => {
    knex(config)
        .select("*")
        // .select("id", "name", "isActive", "isDelete")
        .from("todos")
        .where({ isActive: true, isDelete: false })
        .then(function(todos) {
            res.send(todos);
        })
        .error(e => res.status(500).json(e.message));
});

router.get("/complete", (req, res) => {
    knex(config)
        .select("*")
        .from("todos")
        .where({ isActive: false, isDelete: false })
        .then(function(todos) {
            res.send(todos);
        })
        .error(e => res.status(500).json(e.message));
});

router.delete("/v2/:id", (req, res) => {
    knex(config)
        .where("id", req.params.id)
        .update({ isDelete: false })
        .from("todos")
        .then(function(todos) {
            res.sendStatus(200);
        })
        .error(e => res.status(500).json(e.message));
});

router.post("/v2", (req, res) => {
    knex(config)
        .returning("*")
        .insert(req.body)
        .into("users")
        .then(function(data) {
            res.send(data);
        })
        .error(e => res.status(500).json(e.message));
});

router.post("/v3", (req, res) => {
    knex(config)
        .where({
            Email_id: req.body.Email_id,
            password: req.body.password
        })
        .from("users")
        .then(function(data) {
            res.send(data);
        })
        .error(e => res.status(500).json(e.message));
});

// router.get("/", (req, res) => {
//     res.status(200).json(todos);
// });

// router.post("/", (req, res) => {
//     const newTodo = req.body;
//     count = count + 1;
//     newTodo.id = count;
//     todos.push(newTodo);
//     res.status(201).json(todos);
// });

// router.put("/:id", (req, res) => {
//     const id = req.params.id;
//     if (todos[id]) {
//         const updatedTodo = req.body;
//         todos[id] = updatedTodo;
//         res.status(200).send();
//     } else {
//         res.status(404).send();
//     }
// });

// router.delete("/:id", (req, res) => {
//     const id = parseInt(req.params.id);
//     if (todos.filter(todo => todo.id == id).length !== 0) {
//         todos = todos.filter(todo => todo.id !== id);
//         response.status(200).send();
//     } else {
//         response.status(404).send();
//     }
// });

// router.delete("/:id", (req, res) => {
//     res.status(200).send(todos.filter(todo => !todo.isDelete));
// });

// router.get("/complete", (req, res) => {
//     res.status(200).send(todos.filter(todo => !todo.isActive && todo.isDelete));
// });

// router.get("/active", (req, res) => {
//     res.status(200).send(todos.filter(todo => todo.isActive && todo.isDelete));
// });
module.exports = router;